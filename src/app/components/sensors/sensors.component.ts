import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Sensor } from 'src/app/model/models';
import { SensorService } from 'src/app/services/sensor.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-sensors',
  templateUrl: './sensors.component.html',
  styleUrls: ['./sensors.component.css']
})

export class SensorsComponent {
  sensors: Sensor[] = []

  constructor(private sensorsService: SensorService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    const routeParams = this.route.snapshot.paramMap
    const deviceId = routeParams.get('deviceId')
    console.log(deviceId)
    if (deviceId) {
      this.sensorsService.findSensorsByDevice(deviceId).subscribe({
        next: sensors => this.sensors = sensors
      })
    }
  }
}
