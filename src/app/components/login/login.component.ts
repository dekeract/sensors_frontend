import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Credentials } from 'src/app/model/models';
import { HttpAuthService } from 'src/app/services/http-auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent {
  constructor(
    private httpAuthServ: HttpAuthService,
    private toastr: ToastrService,
    private router: Router,
  ) {
    this.serviceId = null;
    this.redirectUrl = null;
  }

  serviceId: string | null;
  redirectUrl: string | null;

  ngOnInit(): void {
  }

  credentialsForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  });

  onSubmit() {
    let username: string = this.credentialsForm.value.username ?? '';
    let password: string = this.credentialsForm.value.password ?? '';
    const credentials = new Credentials(username, password)
    this.httpAuthServ.login(credentials).subscribe({
      next: () => {
        this.toastr.success('Login successful.');
        this.router.navigate(['/']);
      },
      error: () => {
        this.toastr.error('Invalid credentials');
      },
    });
  }
}
