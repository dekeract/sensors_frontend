import { Component } from '@angular/core';
import { Device } from 'src/app/model/models';
import { DeviceService } from 'src/app/services/device.service';

@Component({
  selector: 'app-devices',
  templateUrl: './devices.component.html',
  styleUrls: ['./devices.component.css']
})
export class DevicesComponent {
  devices: Device[] = []

  constructor(private deviceService: DeviceService) {
  }

  ngOnInit(): void {
    this.deviceService.getAllDevices().subscribe({
      next: devices => this.devices = devices
    })
  }
}
