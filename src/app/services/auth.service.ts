import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  deleteToken() {
    localStorage.removeItem('jwt');
  }
  getToken(): string | null {
    return localStorage.getItem('jwt');
  }

  isLoggedIn(): boolean {
    return localStorage.getItem('jwt') !== null;
  }

  getPrincipal(): string {
    let token = this.parseToken();
    if (token) {
      return token.sub;
    }
    return '';
  }

  isJwtValid(): boolean {
    let claims = this.parseToken();
    let now = new Date();
    if (claims) {
      return (
        claims.exp.getTime() > now.getTime() &&
        claims.iat.getTime() < now.getTime()
      );
    }
    return false;
  }

  private parseToken(): RegisteredClaims | null {
    let jwt = localStorage.getItem('jwt');
    try {
      if (jwt !== null) {
        let jwtData = jwt.split('.')[1];
        let decodedJwtJsonData = window.atob(jwtData);
        let decodedJwtData = JSON.parse(decodedJwtJsonData);
        let claims: RegisteredClaims = {
          sub: decodedJwtData.sub,
          exp: new Date(decodedJwtData.exp * 1_000),
          iat: new Date(decodedJwtData.iat * 1_000),
          rol: decodedJwtData.rol
        };
        return claims;
      }
    } catch (error) {
      return null;
    }
    return null;
  }
}

interface RegisteredClaims {
  sub: string;
  exp: Date;
  iat: Date;
  rol: string;
}
