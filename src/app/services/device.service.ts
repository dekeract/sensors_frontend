import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Device } from '../model/models';

@Injectable({
  providedIn: 'root'
})
export class DeviceService {
  constructor(private http: HttpClient) { }

  getAllDevices(): Observable<Device[]> {
    let url = new URL(environment.apiUrl);
    url.pathname = 'api/devices/';
    return this.http.get<Device[]>(url.toString())
  }
}
