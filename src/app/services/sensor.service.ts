import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Sensor } from '../model/models';

@Injectable({
  providedIn: 'root'
})
export class SensorService {
  constructor(private http: HttpClient) { }

  findSensorsByDevice(deviceId: string): Observable<Sensor[]> {
    let url = new URL(environment.apiUrl);
    url.pathname = `/api/devices/${deviceId}/sensors/`;
    return this.http.get<Sensor[]>(url.toString())
  }
}
