import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Credentials } from '../model/models';

export interface JWTResponse {
  jwt: string;
}

@Injectable({
  providedIn: 'root'
})
export class HttpAuthService {
  constructor(private http: HttpClient) { }

  login(credentials: Credentials): Observable<string> {
    let url = new URL(environment.apiUrl);
    url.pathname = 'api/auth/login';
    return this.http
      .post<JWTResponse>(url.toString(), credentials)
      .pipe(map((jr) => jr.jwt), tap((jwt) => localStorage.setItem('jwt', jwt)))
      ;
  }
}

