import { inject, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DevicesComponent } from './components/devices/devices.component';
import { LoginComponent } from './components/login/login.component';
import { SensorsComponent } from './components/sensors/sensors.component';
import { AuthService } from './services/auth.service';

const routes: Routes = [
  {
    path: 'login', pathMatch: 'full',
    canActivate: [() => !inject(AuthService).isJwtValid()],
    component: LoginComponent
  },
  {
    path: '',
    canActivate: [() => inject(AuthService).isJwtValid()],
    component: DevicesComponent
  },
  {
    path: 'devices/:deviceId/sensors',
    canActivate: [() => inject(AuthService).isJwtValid()],
    component: SensorsComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
