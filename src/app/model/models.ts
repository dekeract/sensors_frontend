export class Credentials {
  constructor(public readonly username: string, public readonly password: string) {
  }
}

export class Device {
  constructor(public readonly id: string, public readonly owner: string, public readonly name?: string) { }
}

export class Sensor {
  constructor(
    public readonly id: string,
    public readonly name: string,
    public readonly unit: string,
    public readonly deviceId: string,
    public readonly type: string,
  ) { }
}
